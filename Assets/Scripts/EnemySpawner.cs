﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private Vector2 _spawnRangeX;
    [SerializeField] private Vector2 _spawnRangeY;
    [SerializeField] private Vector2 _spawnRangeTime;
    [SerializeField] private GameObject[] _enemyPrefabs;

    
    void Start()
    {
        StartCoroutine(spawn());
    }
    
    void Update()
    {
        
    }

    private IEnumerator spawn()
    {
        while (true)
        {
            float spawnPosX = Random.Range(_spawnRangeX.x, _spawnRangeX.y);
            float spawnPosY = Random.Range(_spawnRangeY.x, _spawnRangeY.y);
            int iterator = Random.Range(0, _enemyPrefabs.Length);
            float timeSpawn = Random.Range(_spawnRangeTime.x, _spawnRangeTime.y);
            float randAngleY = Random.Range(-180f, 180f);

            Debug.Log("Angle Y: " + randAngleY);
            GameObject GO = Instantiate(_enemyPrefabs[iterator], new Vector3(spawnPosX, 0.25f, spawnPosY),
                Quaternion.identity);
            GO.transform.rotation = Quaternion.Euler(0, randAngleY, 0);
            GameSceneManager.S.EnemyCounter += 1;
            //Debug.Log("Enemy count = " + GameSceneManager.S.enemyCounter);
            //Debug.Log("Iterator = " + iterator);
            

            yield return new WaitForSeconds(timeSpawn);
        }
    }
}
