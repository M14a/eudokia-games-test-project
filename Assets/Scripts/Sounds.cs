using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sounds : MonoBehaviour
{
    public static Sounds S;

    public AudioClip _hitSound;
    public AudioClip _destroySound;
    public AudioClip _collisionSound;
    public AudioClip _menuSound;

    private AudioSource _audioSource;

    private void Awake()
    {
        S = this;
    }

    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }
    
    void Update()
    {
        
    }

    public void playSound(AudioClip audioClip)
    {
        _audioSource.clip = audioClip;
        _audioSource.Play();
    }
}
