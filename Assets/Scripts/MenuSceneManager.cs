using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuSceneManager : MonoBehaviour
{
    //--Menu------Key---
    //Main menu = 0
    //Score     = 1
    //Credits   = 2
    [SerializeField] private List<Transform> _menus = new List<Transform>();
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void OnClickPlayButton()
    {
        Sounds.S.playSound(Sounds.S._menuSound);
        SceneManager.LoadScene("GameScene");
    }

    public void OnClickExitButton()
    {
        Sounds.S.playSound(Sounds.S._menuSound);
        Application.Quit();
    }

    public void OnClickScoreButton()
    {
        Sounds.S.playSound(Sounds.S._menuSound);
        updateScore();
        changeMenu(1);
    }

    public void OnClickCreditsButton()
    {
        Sounds.S.playSound(Sounds.S._menuSound);
        changeMenu(2);
    }

    public void OnClickBackMainMenuButton()
    {
        Sounds.S.playSound(Sounds.S._menuSound);
        changeMenu(0);
    }

    private void changeMenu(int key)
    {
        for (int i = 0; i < _menus.Count; i++)
        {
            _menus[i].gameObject.SetActive(false);
            if (i == key)
            {
                _menus[i].gameObject.SetActive(true);
            }
        }
    }

    private void updateScore()
    {
        Text description = _menus[1].Find("Description").gameObject.GetComponent<Text>();
        description.text = "Лучшая попытка: " + PlayerPrefs.GetInt("Score");
    }
    
}
