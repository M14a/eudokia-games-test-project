using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameSceneManager : MonoBehaviour
{
    static public GameSceneManager S;

    [SerializeField] private Text _loseText;
    [SerializeField] private Text _scoreText;
    [SerializeField] private GameObject _explosion;
    [SerializeField] private Transform _pauseTransform;
    
    private int _enemyCounter = 0;
    private int _score = 0;

    public bool IsPaused { get; set; } = false;

    public GameObject Explosion
    {
        get { return _explosion; }
    }

    public int EnemyCounter
    {
        get { return _enemyCounter; }
        set { _enemyCounter = value; }
    }

    public int Score
    {
        get { return _score; }
        set { _score = value; }
    }
    
    private void Awake()
    {
        S = this;
    }

    void Start()
    {
        
    }
    
    void Update()
    {
        _scoreText.text = "Счет: " + _score;
        if (_enemyCounter >= 10)
        {
            _loseText.gameObject.SetActive(true);
            Time.timeScale = 0;
            StartCoroutine(loseGame());
        }
        
        
    }

    private IEnumerator loseGame()
    {
        if (_score > PlayerPrefs.GetInt("Score"))
        {
            PlayerPrefs.SetInt("Score", _score);
        }
        
        yield return new WaitForSecondsRealtime(3.0f);
        
        Time.timeScale = 1;
        _loseText.gameObject.SetActive(false);
        SceneManager.LoadScene("GameScene");
    }

    public void OnClickPauseButton()
    {
        IsPaused = true;
        Sounds.S.playSound(Sounds.S._menuSound);
        _pauseTransform.gameObject.SetActive(true);
        Time.timeScale = 0;
    }

    public void OnClickBackToMenuButton()
    {
        Sounds.S.playSound(Sounds.S._menuSound);
        SceneManager.LoadScene("MenuScene");
    }

    public void OnClickBackToGameButton()
    {
        IsPaused = false;
        Sounds.S.playSound(Sounds.S._menuSound);
        _pauseTransform.gameObject.SetActive(false);
        Time.timeScale = 1;
    }
}
