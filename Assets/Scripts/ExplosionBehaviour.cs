using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionBehaviour : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(destroy());
    }
    
    void Update()
    {
        
    }

    private IEnumerator destroy()
    {
        yield return new WaitForSeconds(2f);
        
        Destroy(gameObject);
    }
}
