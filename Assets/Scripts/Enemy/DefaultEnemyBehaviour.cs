﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultEnemyBehaviour : MonoBehaviour
{
    [SerializeField] protected int _tapsToDestroy;

    private int _currentTaps = 0;
    public int CurrentTaps
    {
        get { return _currentTaps; }
        set { _currentTaps = value; }
    }
    void Start()
    {
        
    }

    void Update()
    {
        checkCountTapsToDestroy();
    }

    protected void checkCountTapsToDestroy()
    {
        if (CurrentTaps >= _tapsToDestroy)
        {
            Sounds.S.playSound(Sounds.S._destroySound);
            GameSceneManager.S.EnemyCounter -= 1;
            GameSceneManager.S.Score += 1;
            Debug.Log("Enemy destroyed! \nEnemy count = " + GameSceneManager.S.EnemyCounter);
            Instantiate(GameSceneManager.S.Explosion, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
    
}
