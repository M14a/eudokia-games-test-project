using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerInput : MonoBehaviour
{
    private Touch _touch;
    void Start()
    {

    }
    
    void Update()
    {
        if (Input.touchCount > 0)
        {
            _touch = Input.GetTouch(0);

            if (_touch.phase == TouchPhase.Began && GameSceneManager.S.IsPaused == false)
            {
                Ray raycast = Camera.main.ScreenPointToRay(_touch.position);
                RaycastHit raycastHit;
                if (Physics.Raycast(raycast, out raycastHit))
                {
                    if (raycastHit.collider.gameObject.tag == "Enemy")
                    {
                        Sounds.S.playSound(Sounds.S._hitSound);
                        Debug.Log("Hit enemy");
                        raycastHit.collider.gameObject.GetComponent<DefaultEnemyBehaviour>().CurrentTaps += 1;
                    }
                }
            }
        }
    }
}
