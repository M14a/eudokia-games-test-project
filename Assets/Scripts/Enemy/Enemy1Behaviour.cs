using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy1Behaviour : DefaultEnemyBehaviour
{
    [SerializeField] private float _speed;
    
    private Vector2 _angleRotationRange = new Vector2(-90, 90);
    private Rigidbody rb;

    private void Awake()
    {
        
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    
    void Update()
    {
        checkCountTapsToDestroy();
        Move();
    }

    private void Move()
    {
        Vector3 targetVector = transform.position + transform.forward * _speed * Time.deltaTime;
        //transform.position = targetVector;
        rb.MovePosition(targetVector);
    }

    private void ChangeAngle()
    {
        float angle = Random.Range(_angleRotationRange.x, _angleRotationRange.y);
        transform.Rotate(Vector3.up, angle, Space.Self);
    }

    private void OnCollisionEnter(Collision other)
    {
        Sounds.S.playSound(Sounds.S._collisionSound);
        if (other.gameObject.tag == "Wall" || other.gameObject.tag == "Enemy")
        {
            ChangeAngle();
        }
    }

    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.tag == "Wall" || other.gameObject.tag == "Enemy")
        {
            ChangeAngle();
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "Wall" || other.gameObject.tag == "Enemy")
        {
            
        }
    }
}
